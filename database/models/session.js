'use strict';
module.exports = (sequelize, DataTypes) => {
  const session = sequelize.define('session', {
    sid: DataTypes.STRING,
    expires: DataTypes.STRING,
    data: DataTypes.STRING,
  }, {});
  session.associate = function (models) {
    // associations can be defined here
  };
  return session;
};