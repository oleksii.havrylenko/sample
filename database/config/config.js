module.exports = {
  database: process.env.DB_NAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT || 5432,
  dialect: 'postgres',
  dialectOptions: {
    ssl: false,
  },
  define: {
    timestamps: true,
  },
  freezeTableName: true,
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
  logging: false, // process.env.NODE_ENV === 'development' ? console.log : false // Does sequelize should log all requests?
  // Use a different schema for the SequelizeMeta table
  migrationStorageTableSchema: "sequelize_meta",
  // Use a different table name. Default: SequelizeData
  seederStorageTableName: "sequelize_data",
};
