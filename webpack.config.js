const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const isDevelopment = process.env.NODE_ENV === 'development';
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

// common
const loaders = [];
const plugins = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.PROTOCOL': JSON.stringify(process.env.PROTOCOL),
    'process.env.HOSTNAME': JSON.stringify(process.env.HOSTNAME),
  }),
];

const config = {
  mode: isDevelopment ? 'development' : 'production',
  devtool: isDevelopment ? 'inline-source-map' : undefined,
  target: 'web',
  output: {
    publicPath: '/',
    filename: '[name].js',
    path: path.resolve(__dirname, 'public'),
  },
  entry: ['./client/index.jsx'],
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  optimization: {
    noEmitOnErrors: true,
    minimizer: [],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: !isDevelopment,
      debug: isDevelopment,
      noInfo: true,
    }),
  ],
  module: {
    rules: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader" },
    ],
  },
};

if (isDevelopment) {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  );

  loaders.push(
    // convertToAbsoluteUrls - https://github.com/webpack-contrib/style-loader#url
    // when style loader is used with { options: { sourceMap: true } } option, the CSS modules will be generated as Blobs, so relative paths don't work
    {
      test: /\.(sc|c)ss$/,
      use: [
        'style-loader?convertToAbsoluteUrls',
        { loader: 'css-loader', options: { modules: 'global', importLoaders: 1, sourceMap: true } },
        { loader: 'postcss-loader', options: { config: { path: './postcss.config.js' }, sourceMap: true } },
        'sass-loader?sourceMap'],
    },
  );

  config.entry.push('webpack-hot-middleware/client', 'webpack/hot/dev-server');
} else {
  config.optimization.minimizer.push(
    new UglifyJSPlugin({
      exclude: /node_modules/,
      sourceMap: false,
      uglifyOptions: {
        ecma: 5,
        ie8: true,
        output: {
          comments: false,
          beautify: false,
        },
        compress: {
          drop_console: true,
          drop_debugger: true,
        },
      },
      parallel: true,
    }),
  );

  plugins.push(
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin('[name].css'),
  );

  loaders.push(
    {
      test: /\.(sc|c)ss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          { loader: 'css-loader' },
          { loader: 'postcss-loader', options: { modules: 'local', config: { path: './postcss.config.js' } } },
          { loader: 'sass-loader' },
        ],
      }),
    },
  );
}

config.module.rules.push(...loaders);
config.plugins.push(...plugins);

module.exports = config;