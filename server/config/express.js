const express = require('express');
const session = require('express-session');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');
const path = require('path');
const helmet = require('helmet');
const cluster = require('cluster');
const config = require('./index');
const logger = require('../../shared/utils/logger');
const db = require('../../database/models');
const AWS = require('aws-sdk');
AWS.config = config.aws;

// initalize sequelize with session store
const SequelizeStore = require('connect-session-sequelize')(session.Store);

const { NODE_ENV: nodeEnv } = process.env;

const app = express();

if (nodeEnv === 'development') {
  const webpack = require('webpack');
  const browserWebpackConfig = require('../../webpack.config.js');
  const compiler = webpack(browserWebpackConfig);

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: browserWebpackConfig.output.publicPath,
    watchOptions: {
      poll: true,
      ignored: /node_modules/,
    },
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}

app.set('port', (process.env.PORT || 80));

app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.xssFilter());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noCache());
app.use(helmet.noSniff());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express.static(path.join(__dirname, '../../public')));

// The trust proxy setting is implemented using the proxy-addr package. For more information, see its documentation.
// loopback - 127.0.0.1/8, ::1/128
app.set('trust proxy', 'loopback');

const sequelizeStore = new SequelizeStore({
  db: db,
  table: 'session',
});

sequelizeStore.sync();

// Create a session middleware with the given options
// Note session data is not saved in the cookie itself, just the session ID. Session data is stored server-side.

let sess = {
  resave: true,
  saveUninitialized: false,
  secret: config.sessionSecret,
  proxy: true, // The "X-Forwarded-Proto" header will be used.
  name: 'sessionId',
  store: sequelizeStore,
};

app.use(session(sess));

if (nodeEnv === 'production') {
  // Basic auth
  app.use((req, res, next) => {
    if (req.ip.replace(/::ffff:/, '') === '127.0.0.1') {
      return next();
    }
    const { basicAuth: { username, password } = {} } = config;
    return basicAuth({
      users: { [username]: password },
      challenge: true,
      realm: 'Authentication required',
    })(req, res, next);
  });
}

// Bootstrap routes
require('../routes/index')(app);

logger.debug(`Worker ${cluster.worker.id} has been started!`);

module.exports = app;
