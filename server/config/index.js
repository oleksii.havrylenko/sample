module.exports = {
  sessionSecret: process.env.SESSION_SECRET || 'sessionSecret',
  basicAuth: {
    username: process.env.BASIC_AUTH_USERNAME,
    password: process.env.BASIC_AUTH_PASSWORD,
  },
  aws: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.REGION,
    output: process.env.OUTPUT,
    s3: '2016-04-18',
    buckets: {
      sample: process.env.BACKUP_BUCKET,
    },
  },
};
