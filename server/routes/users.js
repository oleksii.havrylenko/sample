const express = require('express');
const models = require('../../database/models');
const Users = require('../controllers/Users')(models);
const Auth = require('../../shared/Auth')('Users');

const router = express.Router();

// CRUD
router.route('/')
  .get(Auth.serverCheckPermissionOn('read'), Users.fetch)
  .post(Auth.serverCheckPermissionOn('create'), Users.create);

router.route('/:id')
  .get(Auth.serverCheckPermissionOn('read'), Users.fetchById)
  .put(Auth.serverCheckPermissionOn('update'), Users.update)
  .delete(Auth.serverCheckPermissionOn('delete'), Users.delete);

module.exports = router;
