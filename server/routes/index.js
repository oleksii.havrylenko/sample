const _ = require('lodash');
const ssr = require('../controllers/ssr');
const logger = require('../../shared/utils/logger');
const Auth = require('../controllers/Auth')();

const auth = require('./auth');

module.exports = (app) => {

  app.use('/api/auth', auth);

  app.get('*', (req, res, next) => {
    ssr(req, res, next, process);
  });

// error handler
// development error handler
// will print stacktrace
// production error handler
// no stacktraces leaked to user
  app.use((err, req, res, next) => {
    logger.error('WEB', err);
    if ( err.errors ) {
      err.message = err.errors.map(e => _.get(e, 'message'));
    }
    res.setHeader('content-type', 'application/json');
    res.status(err.status || 500);
    res.json({
      response: {
        type: 'error',
        message: err.message,
      },
      error: app.get('env') === 'development' ? err : { status: err.status },
    });
  });

};
