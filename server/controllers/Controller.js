class Controller {

  constructor(db = {}) {
    this._db = db;
    const func = this.action;

    this.action = async (req, res, next) => {
      try {
        await this.validate(req, res, next);
        await func.call(this, req, res, next);
      } catch ( ex ) {
        next(ex);
      }
    };

  }

  /**
   * abstract
   * @param req
   * @param res
   * @param next
   * @returns {Promise<void>}
   */
  async validate(req, res, next) {

  }

  /**
   * abstract
   * @param req
   * @param res
   * @param next
   * @returns {Promise<void>}
   */
  async action(req, res, next) {
    throw new Error(`Method 'action' not implemented.`);
  }

}

module.exports = Controller;
