const _ = require('lodash');
const Controller = require('../Controller');

class Fetch extends Controller {

  async action(req, res, next) {
    try {
      const { limit, offset } = req.query;

      res.json({
        items: [],
        totalCount: 0,
      });
    } catch ( ex ) {
      next(ex);
    }
  }

}

module.exports = Fetch;
