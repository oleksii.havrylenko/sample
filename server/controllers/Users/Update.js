const _ = require('lodash');
const Controller = require('../Controller');

class Update extends Controller {

  async action(req, res, next) {
    try {

      res.json({ response: { type: 'success', message: 'Updated' }, item: {} });
    } catch ( ex ) {
      next(ex);
    }
  }

}

module.exports = Update;
