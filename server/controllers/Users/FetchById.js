const _ = require('lodash');
const Controller = require('../Controller');

class FetchById extends Controller {

  async action(req, res, next) {
    try {
      const { id } = req.params;

      res.json({ item: {} });
    } catch ( ex ) {
      next(ex);
    }
  }

}

module.exports = FetchById;
