const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const controller = {};

const files = fs.readdirSync(__dirname);
files.forEach(fileName => {

  const fullPath = path.join(__dirname, fileName);
  if (fs.statSync(fullPath).isDirectory()) {
    return _.merge(controller, require(fullPath));
  }

  const baseName = path.basename(fileName, '.js');
  const extension = path.extname(fileName);

  if (extension !== '.js' || /((^index)|(\.test))/.test(baseName)) {
    return;
  }
  controller[baseName] = require(fullPath);
});

module.exports = function (db) {
  return _.reduce(controller, (result, Controller, name) => {
    result[_.camelCase(name)] = new Controller(db).action;
    return result;
  }, {});
};