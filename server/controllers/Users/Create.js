const _ = require('lodash');
const Controller = require('../Controller');

class Create extends Controller {

  async action(req, res, next) {
    try {
      const userData = req.body;
      const user = await this._db.user.create(userData);

      res.json({ response: { type: 'success', message: 'Created' }, item: user });
    } catch ( ex ) {
      next(ex);
    }
  }

}


module.exports = Create;
