const _ = require('lodash');
const Controller = require('../Controller');

class Delete extends Controller {

  async action(req, res, next) {
    try {
      const { id } = req.params;

      if (req.user.id === id) {
        return res.status(400).json({ response: { type: 'error', message: 'You can not delete yourself' } });
      }

      await this._db.user.destroy({ where: { id: id } });

      res.json({ response: { type: 'success', message: 'Deleted' }, deletedId: id });
    } catch ( ex ) {
      next(ex);
    }
  }
}

module.exports = Delete;
