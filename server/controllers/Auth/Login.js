const _ = require('lodash');
const util = require('util');
const logger = require('../../../shared/utils/logger');

const Controller = require('../Controller');

class Login extends Controller {

  async action(req, res, next) {
    try {
      res.end();
    } catch ( ex ) {
      next(ex);
    }
  }
}

module.exports = Login;
