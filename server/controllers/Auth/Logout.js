const _ = require('lodash');
const Controller = require('../Controller');

class Logout extends Controller {

  async action(req, res, next) {
    try {
      res.end();
    } catch ( ex ) {
      next(ex);
    }
  }
}

module.exports = Logout;
