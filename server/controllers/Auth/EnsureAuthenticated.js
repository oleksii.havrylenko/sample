const _ = require('lodash');

const Controller = require('../Controller');

class EnsureAuthenticated extends Controller {

	async action(req, res, next) {
		try {
			// if ( !req.isAuthenticated() ) {
			// 	return res.status(401).json({ response: { type: 'error', message: 'Not Authorized.' } });
			// }

			return next();
		} catch ( ex ) {
			next(ex);
		}
	}

}

module.exports = EnsureAuthenticated;
