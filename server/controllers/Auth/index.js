const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const controller = {};

const files = fs.readdirSync(__dirname);
files.forEach(fileName => {

  const fullPath = path.join(__dirname, fileName);
  if (fs.statSync(fullPath).isDirectory()) {
    return _.merge(controller, require(fullPath));
  }

  const baseName = path.basename(fileName, '.js');
  const extension = path.extname(fileName);

  if (extension !== '.js' || /((^index)|(\.test))/.test(baseName)) {
    return;
  }
  controller[baseName] = require(fullPath);
});

/**
 * Checks if user has required permission for required request
 * @param {string} permissionRequired
 * @param {string} accessRequired
 * @returns {function(*,*,*)}
 */
function hasPermission(permissionRequired, accessRequired) {
  return (req, res, next) => {
    return true;
    // 	if ( req.user.roles.some(role =>
    // 		role.permissions.some(permission => permission.key === permissionRequired
    // 			&& (permission.role_permission.access === accessRequired || permission.role_permission.access === 'write'))) ) {
    // 		return next();
    // 	}
    // 	return res.status(403).json({ response: { type: 'error', message: 'Permission denied.' } });
  };
}

module.exports = function (db) {
  const result = _.reduce(controller, (result, Controller, name) => {
    result[_.camelCase(name)] = new Controller(db).action;
    return result;
  }, {});

  result.hasPermission = hasPermission;
  return result;
};