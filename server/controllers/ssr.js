/*
 * Our html template file
 * @param {String} renderedContent
 * @param initial state of the store, so that the client can be hydrated with the same state as the server
 * @param head - optional arguments to be placed into the head
 */
function renderFullPage(initialState, serverData, head = {
  title: '<title>Sample</title>',
  meta: '<meta name="viewport" content="width=device-width, initial-scale=1" />' +
    '<meta charset="utf-8">',
  link: '<link rel="stylesheet" href="/main.css"/>',
}) {
  return `
  <!doctype html>
    <html lang="en">

    <head>
        ${head.title}

        ${head.meta}

        ${head.link}
    </head>
    <body>
    <div id="app"/>

    <script>
      window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
      window.server = ${JSON.stringify(serverData)};
    </script>
    <script type="text/javascript" charset="utf-8" src="/main.js"></script>
    </body>
    </html>`;
}

/*
 * Export render function to be used in server/routes/index.js
 * We grab the state passed in from the server and the req object from Express/Koa
 * and pass it into the Router.run function.
 */
module.exports = (req, res, next, serverProcess) => {
  try {
    const port = serverProcess.env.PORT || 80;

    const initialState = {
      auth: {
        user: {},
      },
    };

    // if (req.isAuthenticated()) {
    //   initialState = initialState.auth.user = _.omit(req.user, 'password');
    // }

    const serverData = {
      port: port,
      host: req.hostname,
      secure: serverProcess.env.NODE_ENV === 'production',
    };

    res.header('Content-Type', 'text/html').status(200).end(renderFullPage(initialState, serverData));

  } catch ( e ) {
    console.error(e);
    res.end(e);
  }
};
