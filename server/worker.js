const http = require('http');
const cluster = require('cluster');
const debug = require('debug')('sample:server');
const app = require('./config/express');
const { logger } = require('../shared/utils');

// Workers can share any TCP connection
// In this case it is an HTTP server
const server = http.createServer(app).listen(app.get('port'));
server.on('error', onError);
server.on('listening', onListening.bind(null, server));
server.timeout = 0;

function onError(error) {
  if (!['listen', 'bind'].includes(error.syscall)) {
    throw error;
  }
  const port = app.get('port');
  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch ( error.code ) {
    case 'EACCES': {
      logger.error(bind + ' requires elevated privileges');
      if (cluster.isWorker) {
        cluster.worker.send({ type: 'shutdown', code: 1 });
      }
    }
      break;
    case 'EADDRINUSE': {
      logger.error(bind + ' is already in use');
      if (cluster.isWorker) {
        cluster.worker.send({ type: 'shutdown', code: 1 });
      }
    }
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening(server) {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
