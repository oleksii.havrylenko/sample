module.exports = [
  {
    routes: [],
    types: [
      {
        name: 'create',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'delete',
        permission: '',
        access: 'write',
      },
      {
        name: 'import',
        permission: '',
        access: 'write',
      },
    ],
  },
  {
    route: '',
    types: [
      {
        name: 'create',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'delete',
        permission: '',
        access: 'write',
      },
    ],
  },
  {
    route: 'Users',
    types: [
      {
        name: 'create',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'delete',
        permission: '',
        access: 'write',
      },
    ],
  },
  {
    routes: [],
    types: [
      {
        name: 'create',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'delete',
        permission: '',
        access: 'write',
      },
      {
        name: 'import',
        permission: '',
        access: 'write',
      },
    ],
  },
  {
    route: '',
    types: [
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'import',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
    ],
  },
  {
    route: '',
    types: [
      {
        name: 'create',
        permission: '',
        access: 'write',
      },
      {
        name: 'read',
        permission: '',
        access: 'read',
      },
      {
        name: 'update',
        permission: '',
        access: 'write',
      },
      {
        name: 'delete',
        permission: '',
        access: 'write',
      },
      {
        name: 'stop',
        permission: '',
        access: 'write',
      },
      {
        name: 'restart',
        permission: '',
        access: 'write',
      },
    ],
  },
];