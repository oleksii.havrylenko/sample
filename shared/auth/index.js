const _ = require('lodash');
const permissions = require('./permissions');

function isHaveAccess(user, action) {
	return _.some(user.roles, role => {
		return _.some(role.permissions, permission => permission.key === _.get(action, 'permission')
			&& (permission.role_permission.access === _.get(action, 'access') || permission.role_permission.access === 'write'));
	});
}

function getActionByName(routeName, actionName) {
	return _.chain(permissions)
		.find(perm => _.includes(_.get(perm, 'routes', _.get(perm, 'route')), routeName))
		.get('types')
		.find(type => type.name === actionName)
		.value();
}

module.exports = function (routeName) {
	return {
		serverCheckPermissionOn: function (actionName) {
			return (req, res, next) => {
				if ( isHaveAccess(req.user, getActionByName(routeName, actionName)) ) {
					return next();
				}
				return res.status(403).json({ response: { type: 'error', message: 'Permission denied.' } });
			};
		},

		webCheckPermissionOn: function (actionName, user = {}) {
			return isHaveAccess(user, getActionByName(routeName, actionName));
		},
	};
};