const fs = require('fs');
const path = require('path');
const winston = require('winston');
require('winston-daily-rotate-file');
const { Syslog } = require('winston-syslog');

let logDir = './logs/';

// create logs directory if necessary
if ( !fs.existsSync(logDir) ) {
	fs.mkdirSync(logDir);
}

const fileTransport = new winston.transports.DailyRotateFile({
	filename: path.join(logDir, 'log'),
	datePattern: 'dd-MM-yyyy.',
	prepend: true,
	level: process.env.DEBUG_LEVEL || 'error'
});

const logger = winston.createLogger({
	transports: [
		new winston.transports.Console({
			colorize: true,
			timestamp: true,
			level: process.env.DEBUG_LEVEL || 'info',
			stderrLevels: [ 'error', 'emerg', 'crit' ],
			prettyPrint: true
		}),
		new Syslog({
			app_name: 'sample'
		}),
		fileTransport
	]
});

logger.setLevels(winston.config.syslog.levels);

module.exports = logger;
