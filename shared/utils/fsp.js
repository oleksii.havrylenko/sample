const _ = require('lodash');
const util = require('util');
const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const fsp = _.reduce(fs, (res, property, key) => {
	if ( _.isFunction(property) && !key.endsWith('Sync') && !key.endsWith('Stream') ) {
		res[ key ] = util.promisify(property);
	}
	return res;
}, _.cloneDeep(fs));

async function mkdir(target, mode) {
	const asyncp = require('./asyncp');
	const isAbsolutePath = path.isAbsolute(target);

	return asyncp.reduce(
		target.split(path.sep),
		isAbsolutePath ? '/' : __dirname,
		async (currentPath, part) => {
			currentPath = path.resolve(currentPath, part);
			try {
				await fsp.stat(currentPath);
			} catch ( ex ) {
				if ( ex.code === 'ENOENT' ) {
					try {
						await util.promisify(fs.mkdir)(currentPath, mode);
					} catch ( ex ) {
						if ( ex.code !== 'EEXIST' ) {
							throw ex;
						}
					}
				}
			}

			return currentPath;
		}
	);
}

async function rmdir(target) {
	return util.promisify(rimraf)(target);
}

fsp.mkdir = mkdir;
fsp.rmdir = rmdir;

module.exports = fsp;
