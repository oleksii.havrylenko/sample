const _ = require('lodash');
const util = require('util');
const async = require('async');

module.exports = _.reduce([
	'each',
	'eachOf',
	'eachLimit',
	'eachSeries',
	'eachOfSeries',
	'eachOfLimit',
	'filter',
	'retry',
	'map',
	'mapSeries',
	'mapValues',
	'mapValuesSeries',
	'reduce',
	'transform',
], (result, value) => {
	result[ value ] = util.promisify(async[ value ]);
	return result;
}, {});