import { createReducer } from 'redux-act';
import authActions from '../actions/auth';
import _ from "lodash";

const initialState = {
  user: {},
};

export default createReducer({
  [authActions.login.ok]: (state, payload) => {
    const newState = _.cloneDeep(state);
    newState.user = payload.user;

    return newState;
  },

  [authActions.logout.ok]: (state, payload) => {
    return initialState;
  },

}, initialState);
