import { createReducer } from 'redux-act';
import _ from 'lodash';
import simpleActions from '../actions/common';

const initialState = {
  response: {},
  amountOfActiveProcesses: 0,
  theme: 'light',
};

export default createReducer({
  [simpleActions.clearResponse]: (state, payload) => {
    const newState = _.cloneDeep(state);
    newState.response = {};

    return newState;
  },

  [simpleActions.catchResponse]: (state, payload) => {
    const newState = _.cloneDeep(state);
    newState.response = payload;

    return newState;
  },

  [simpleActions.addProcess]: (state, payload) => {
    state.amountOfActiveProcesses++;
    return state;
  },

  [simpleActions.removeProcess]: (state, payload) => {
    state.amountOfActiveProcesses--;
    return state;
  },

}, initialState);
