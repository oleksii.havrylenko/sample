import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import catchServerMessages from '../middleware/catchServerMessages';
import manageAmountOfActiveProcesses from '../middleware/manageAmountOfActiveProcesses';

function configureStore(initialState = {}) {
  const middleware = [thunk, catchServerMessages, manageAmountOfActiveProcesses];

  // Installs hooks that always keep react-router and redux store in sync
  const finalCreateStore = process.env.NODE_ENV === 'development'
    ? composeWithDevTools(applyMiddleware(...middleware))(createStore)
    : applyMiddleware(...middleware)(createStore);

  const store = finalCreateStore(rootReducer, initialState);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

export default configureStore;