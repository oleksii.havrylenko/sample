import _ from 'lodash';
import simpleActions from '../actions/common';

export default store => next => action => {
  const { dispatch } = store;
  const result = next(action);
  const actionType = _.get(action, 'type');

  if (_.endsWith(actionType, '_REQUEST')) {
    dispatch(simpleActions.addProcess());
  }

  if (_.endsWith(actionType, '_OK') || _.endsWith(actionType, '_ERROR')) {
    dispatch(simpleActions.removeProcess());
  }

  return result;
};
