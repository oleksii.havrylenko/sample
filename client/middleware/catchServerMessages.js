import _ from 'lodash';
import common from '../actions/common';

export default store => next => action => {
  const { dispatch } = store;
  let result = next(action);
  let response = _.get(action, 'payload.response');

  if (response) {
    dispatch(common.catchResponse(response));
  }
  return result;
};
