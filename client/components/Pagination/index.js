import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from './styles.scss';
import classNames from 'classnames';

class Pagination extends PureComponent {
	static propTypes = {
		activePage: PropTypes.number.isRequired,
		itemsCountPerPage: PropTypes.number.isRequired,
		totalItemsCount: PropTypes.number.isRequired,
		pageRangeDisplayed: PropTypes.number.isRequired,
		onChange: PropTypes.func,
		prevPageText: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.element
		]),
		nextPageText: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.element
		]),
		lastPageText: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.element
		]),
		firstPageText: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.element
		]),
		innerClass: PropTypes.string,
		itemClass: PropTypes.string,
		linkClass: PropTypes.string,
		activeClass: PropTypes.string,
		disabledClass: PropTypes.string,
	};

	static defaultProps = {
		itemsCountPerPage: 25,
		pageRangeDisplayed: 5,
		activePage: 1,
		prevPageText: '⟨',
		firstPageText: '«',
		nextPageText: '⟩',
		lastPageText: '»',
		innerClass: 'pagination',
		activeClass: 'active',
		disabledClass: 'disabled',
		itemClass: 'page'
	};

	state = {};

	onChange(value, isActive) {
		const { activePage, onChange } = this.props;
		if ( !isActive || activePage === value ) {
			return;
		}

		_.isFunction(onChange) && onChange(value);
	}

	render() {
		const { activePage, disabledClass, itemClass } = this.props;

		const pagesCount = Math.ceil(this.props.totalItemsCount / this.props.itemsCountPerPage);
		const delta = this.props.pageRangeDisplayed / 2;
		const deltaFloor = Math.floor(delta);
		let bottom = activePage - deltaFloor;
		let top = activePage + deltaFloor;
		if ( bottom < 1 ) {
			top -= bottom - (deltaFloor !== delta ? 1 : 0);
		}
		if ( top >= pagesCount ) {
			bottom -= top - pagesCount;
		}
		const start = bottom < 1 ? 1 : bottom;
		const end = (top < pagesCount ? top : pagesCount) + 1;
		const pages = _.range(start, end, 1);
		const isFirstDisabled = activePage === 1;
		const isLastDisabled = activePage === pagesCount;

		return <div className={this.props.innerClass}>
			<div className={classNames(itemClass, { [ disabledClass ]: isFirstDisabled })}
			     onClick={this.onChange.bind(this, 1, !isFirstDisabled)}>
				{this.props.firstPageText}
			</div>
			<div className={classNames(itemClass, { [ disabledClass ]: isFirstDisabled })}
			     onClick={this.onChange.bind(this, activePage > 1 ? activePage - 1 : 1, !isFirstDisabled)}>
				{this.props.prevPageText}
			</div>
			{
				_.map(pages, (pageNumber, index) => {
					return (
						<div key={index}
						     className={classNames(itemClass, { [ this.props.activeClass ]: pageNumber === activePage })}
						     onClick={this.onChange.bind(this, pageNumber, true)}>
							{pageNumber}
						</div>
					);
				})
			}
			<div
				className={classNames(this.props.itemClass, { [ this.props.disabledClass ]: isLastDisabled })}
				onClick={this.onChange.bind(this, this.props.activePage < pagesCount ? this.props.activePage + 1 : pagesCount, !isLastDisabled)}>
				{this.props.nextPageText}
			</div>
			<div
				className={classNames(this.props.itemClass, { [ this.props.disabledClass ]: isLastDisabled })}
				onClick={this.onChange.bind(this, pagesCount, !isLastDisabled)}>
				{this.props.lastPageText}
			</div>
		</div>;
	}
}

export default Pagination;
