import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import classNames from 'classnames';

// TODO: local styles
import './styles.scss';

export default class Table extends PureComponent {
	static propTypes = {
		scrollVertical: PropTypes.bool,
		scrollHorizontal: PropTypes.bool || PropTypes.string || PropTypes.number,
		renderRow: PropTypes.func,
		onRowClick: PropTypes.func,
		fields: PropTypes.array,
		items: PropTypes.array,
		hoverable: PropTypes.bool,
		headerNoWrap: PropTypes.bool,
	};

	renderTableRow(fields, item, index, items) {
		const { onRowClick, rowStyle = {}, hoverable = true } = this.props;

		return <div key={index} style={_.isFunction(rowStyle) ? rowStyle(item, index, items) : rowStyle}>
			<div
				className={classNames('group-horizontal table-body-row', { pointer: onRowClick, hoverable })}
				onTouchTap={onRowClick && onRowClick.bind(null, _.cloneDeep(item), index)}
			>
				{
					_.map(fields, (field = {}) => {

						const {
							key, type, style, needRenderColumn = true
						} = _.mapValues(_.cloneDeep(field), (v, k) => _.isFunction(v) ? v(item, field) : v);

						let value = _.get(item, key, null);

						if ( type === 'date' && moment(value).isValid() ) {
							value = moment(value).format('YYYY-MM-DD');
						}

						if ( type === 'time' && moment(value).isValid() ) {
							value = moment(value).format('HH:mm:ss');
						}

						if ( type === 'datetime' && moment(value).isValid() ) {
							value = moment(value).format('YYYY-MM-DD HH:mm:ss');
						}

						return needRenderColumn && <div
							key={key}
							className={classNames('cell center group-vertical', _.get(field, 'className'))}
							style={_.assign({ flex: _.get(field, 'size', 1) }, style)}
						>
							{_.isFunction(field.renderView) && field.renderView(item, field) || _.defaultTo(value, '-')}
						</div>;
					})
				}
			</div>
		</div>;
	}

	renderTable() {
		const { items = [], fields = [], renderRow, scrollHorizontal, style = {}, headerNoWrap, className } = this.props;

		let tableWidth = 'fit-content';

		if ( !_.isBoolean(scrollHorizontal) ) {
			tableWidth = scrollHorizontal;
		}

		return <div
			style={style}
			className={classNames('group-vertical custom-table', { 'scroll-horizontal': _.has(this.props, 'scrollHorizontal') }, className)}
		>
			<div className={'table-inner-wrapper'} style={{ width: tableWidth }}>
				<div className="table-header group-horizontal">
					<div className="table-body-row">
						{
							_.map(fields, field => {
								const title = _.get(field, 'title');
								const key = _.get(field, 'key');
								const needRenderColumn = _.get(field, 'needRenderColumn', true);

								return (_.isFunction(needRenderColumn) ? needRenderColumn() : needRenderColumn) && <div
									key={key}
									className="cell"
									style={{ flex: _.get(field, 'size', 1), whiteSpace: headerNoWrap && 'nowrap' }}
								>
									{_.isFunction(title) && title(field) || title}
								</div>;
							})
						}
					</div>
				</div>
				<div
					className={classNames('table-body group-vertical', { 'scroll-vertical': _.has(this.props, 'scrollVertical') })}
				>
					{
						_.isEmpty(items) &&
						<div style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}>There are no records.</div>
					}
					{_.map(items, (renderRow || this.renderTableRow).bind(this, fields))}
				</div>
			</div>
		</div>;
	}

	render() {
		return this.renderTable();
	}
}

