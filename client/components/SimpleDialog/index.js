import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FlatButton, Dialog } from 'material-ui';
import Formsy from 'formsy-react';
import InputField from '../InputField/index';
import _ from 'lodash';
import classNames from 'classnames';
import styles from './styles.scss';

class SimpleDialog extends PureComponent {
	static propTypes = _.assign({}, Dialog.propTypes, {
		open: PropTypes.bool.isRequired,
		title: PropTypes.string.isRequired,
		fields: PropTypes.array.isRequired,
		data: PropTypes.object,
		submitBtnTitle: PropTypes.string,
		onClose: PropTypes.func,
		onSubmit: PropTypes.func,
		onDelete: PropTypes.func,
		onOpen: PropTypes.func,
		canEdit: PropTypes.bool,
		canDelete: PropTypes.bool,
	});

	state = {
		data: {},
		canSubmit: false,
	};

	get fields() {
		const { fields = [] } = this.props;
		return fields;
	}

	componentWillMount() {
		this.state.data = this.props.data;

		this.onKeyUp = ((ev) => {
			if ( ev.which !== 27 ) {
				return;
			}
			this.onClose();
		}).bind(this);
	}

	componentWillUpdate(nextProps) {
		if ( this.props.open && nextProps.open ) {
			document.onkeyup = this.onKeyUp;
		}
	}

	componentWillReceiveProps(nextProps) {
		const { onOpen } = this.props;

		if ( !this.props.open && nextProps.open ) {
			document.onkeyup = this.onKeyUp;
		} else if ( !nextProps.open && document.onkeyup === this.onKeyUp ) {
			document.onkeyup = null;
		}

		if ( !this.props.open && nextProps.open ) {
			_.isFunction(onOpen) && onOpen();
			this.state.data = _.cloneDeep(nextProps.data);
		}
	}

	toggleButton(value) {
		if ( this.state.canSubmit !== value ) {
			this.setState({ canSubmit: value });
		}
	}

	onSubmit() {
		const { onSubmit } = this.props;
		const { data } = this.state;

		_.isFunction(onSubmit) && onSubmit(data);
	}

	onClose() {
		const { onClose } = this.props;
		_.isFunction(onClose) && onClose();
	}

	onChangeValue(fieldName, ev, value) {
		const { data = {} } = this.state;

		if ( _.isArray(value) ) {
			value = _.compact(value);

			_.set(data, fieldName, []);
		}

		const mergedData = _.merge({}, data, _.set({}, fieldName, value));

		this.setState({ data: mergedData });
	}

	validateSubmit() {
		const { data: initialData } = this.props;
		const { canSubmit, data = {} } = this.state;

		return !canSubmit || _.isEqual(data, initialData);
	}

	get actions() {
		const { submitBtnTitle, onDelete, canDelete = false, canEdit = false } = this.props;
		const { data = {} } = this.state;

		const actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				tabIndex={1}
				onTouchTap={this.onClose.bind(this)}
			/>,
			canEdit && <FlatButton
				label={submitBtnTitle || 'Ok'}
				primary={true}
				tabIndex={0}
				disabled={this.validateSubmit()}
				onTouchTap={this.onSubmit.bind(this)}
			/>,
		];

		if ( _.isFunction(onDelete) && _.get(data, 'id') && canDelete ) {
			actions.unshift(
				<FlatButton
					label={'Delete'}
					primary={true}
					tabIndex={-1}
					onTouchTap={onDelete.bind(null, data)}
				/>,
			);
		}

		return actions;
	}

	render() {
		const { className, bodyClassName } = this.props;
		const { data = {} } = this.state;
		const fields = this.fields;

		return (
			<Dialog
				modal={true}
				repositionOnUpdate={true}
				{...this.props}
				actions={this.actions}
				bodyClassName={classNames(bodyClassName, styles[ 'dialog-body' ])}
				className={classNames(className, styles[ "dialog" ])}
			>
				<Formsy.Form
					onValid={this.toggleButton.bind(this, true)}
					onInvalid={this.toggleButton.bind(this, false)}
				>
					{
						_.map(fields, (field = {}) =>
							<InputField
								key={field.key}
								item={data}
								field={field}
								onChangeValue={(ev, value) => {
									_.isFunction(field.onChange) && field.onChange(ev, value);
									this.onChangeValue(field.key, ev, value);
								}}
							/>,
						)
					}
				</Formsy.Form>
			</Dialog>
		);
	}
}

export default SimpleDialog;
