import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";

import { FormsyDate, FormsyTime } from 'formsy-material-ui/lib';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import { IconButton } from 'material-ui';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class DateTime extends PureComponent {

	static propTypes = _.assign({}, FormsyDate.propTypes, FormsyTime.propTypes, {
		onReset: PropTypes.func,
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string,
	});

	render() {

		const { onReset, description, disabled, required, className, style, title } = this.props;

		const childrenProps = _.omit(this.props, [ 'className', 'style' ]);

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <div
			className={classNames('group-horizontal space-between', className, styles[ 'datetime' ], {
				[ styles[ 'required' ] ]: required,
				[ styles[ 'disabled' ] ]: disabled,
				[ styles[ 'description' ] ]: _.isString(description),
			})}
			style={style}
		>
			<div>
				<div>
					<FormsyDate
						hintText={'Select date'}
						fullWidth={true}
						floatingLabelText={floatingLabelText}
						{...childrenProps}
					/>
					<IconButton
						className={styles[ 'icon' ]}
						onTouchTap={() => _.isFunction(onReset) && onReset()
						}
					>
						<ClearIcon/>
					</IconButton>
				</div>

				<div>
					<FormsyTime
						format='24hr'
						hintText={'Select time'}
						fullWidth={true}
						className={styles[ 'time' ]}
						{...childrenProps}
						floatingLabelText=' '
					/>
					<IconButton
						className={styles[ 'icon' ]}
						onTouchTap={() => _.isFunction(onReset) && onReset()}
					>
						<ClearIcon/>
					</IconButton>
				</div>
			</div>
		</div>;
	}
}