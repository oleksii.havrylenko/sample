import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";

import { FormsyToggle } from 'formsy-material-ui/lib';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Toggle extends PureComponent {

	static propTypes = _.assign({}, FormsyToggle.propTypes, {
		required: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string
	});

	render() {

		const { description, required, className, title } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <FormsyToggle
			label={floatingLabelText}
			{...this.props}
			className={classNames(styles[ 'toggle' ], className, {
				[ styles[ 'required' ] ]: required
			})}
		/>;
	}
}