import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { FormsyTime } from 'formsy-material-ui/lib';

import styles from './styles.scss';
import ClearIcon from "material-ui/svg-icons/content/clear";
import { IconButton } from "material-ui";
import classNames from "classnames";
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Time extends PureComponent {

	static propTypes = _.assign({}, FormsyTime.propTypes, {
		onReset: PropTypes.func,
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string,
	});

	render() {

		const { onReset, required, disabled, description, className, style, title } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <div
			className={classNames('group-horizontal middle', styles[ 'time' ], className, {
				[ styles[ 'required' ] ]: required,
				[ styles[ 'disabled' ] ]: disabled,
				[ styles[ 'description' ] ]: _.isString(description),
			})}
			style={style}
		>
			<FormsyTime
				format='24hr'
				fullWidth={true}
				hintText={'Select time'}
				floatingLabelText={floatingLabelText}
				{..._.omit(this.props, [ 'className', 'style' ])}
			/>
			<IconButton
				className={styles[ 'icon' ]}
				onTouchTap={() => _.isFunction(onReset) && onReset()}
			>
				<ClearIcon/>
			</IconButton>
		</div>;
	}
}