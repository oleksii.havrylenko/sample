import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames';

import { IconButton } from 'material-ui';
import { FormsyDate } from 'formsy-material-ui/lib';
import ClearIcon from 'material-ui/svg-icons/content/clear';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Date extends PureComponent {

	static propTypes = _.assign({}, FormsyDate.propTypes, {
		onReset: PropTypes.func,
		description: PropTypes.string,
		disabled: PropTypes.bool,
		required: PropTypes.bool,
		title: PropTypes.string,
	});

	render() {
		const { onReset, description, disabled, required, className, style, title } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <div
			className={classNames('group-horizontal middle', className, styles[ 'date' ], {
				[ styles[ 'description' ] ]: _.isString(description),
				[ styles[ 'disabled' ] ]: disabled,
				[ styles[ 'required' ] ]: required,
			})}
			style={style}
		>
			<FormsyDate
				hintText={'Select date'}
				floatingLabelFixed={true}
				fullWidth={true}
				floatingLabelText={floatingLabelText}
				{..._.omit(this.props, [ 'className', 'style' ])}
			/>
			<IconButton
				className={styles[ 'icon' ]}
				onTouchTap={() => _.isFunction(onReset) && onReset()}
			>
				<ClearIcon/>
			</IconButton>
		</div>;
	}
}