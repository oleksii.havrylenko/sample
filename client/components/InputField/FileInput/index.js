import React from 'react';
import _ from 'lodash';
import classNames from "classnames";
import PropTypes from 'prop-types';

import { RaisedButton, IconButton } from 'material-ui';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faFileExcel, faFilePdf, faFileWord } from '@fortawesome/free-solid-svg-icons';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

import Formsy from "formsy-react";
import async from "async";

const FileInput = React.createClass({

	mixins: [ Formsy.Mixin ],

	propTypes: {
		onReset: PropTypes.func,
		value: PropTypes.array,
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		title: PropTypes.string,
		description: PropTypes.string,
		onChange: PropTypes.func,
	},

	onReset(ev, file) {
		const { onChange, value: files } = this.props;
		const mappedFiles = _.map(files, f =>
			f.filename === file.filename
			&& f.b64Data === file.b64Data
			&& f.url === file.url
				? null : f);

		_.isFunction(onChange) && onChange(ev, _.isEmpty(_.compact(mappedFiles)) ? null : mappedFiles);
		_.set(this.refs.upload, 'value', null);
	},

	convertToBase64Url(file) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => {
				resolve({
					type: _.get(file, 'type'),
					filename: _.get(file, 'name'),
					b64Data: reader.result,
				});
			};
			reader.onerror = reject;
		});
	},

	onChange(ev, value) {
		const { onChange, value: curValue = [], multiple } = this.props;

		return new Promise((resolve, reject) => {
			async.map(
				value,
				(file, callback) => {
					this.convertToBase64Url(file)
						.then(callback.bind(null, null))
						.catch(callback);
				},
				(err, images) => {
					if ( err ) {
						return reject(err);
					}
					resolve(images);
				});
		})
			.then(images => {
				_.isFunction(onChange) && onChange(ev, _.concat(images, !!multiple ? curValue : []));
			})
			.catch(ex => {
				console.error(ex);
			});
	},

	render() {
		const { value, required, className, title, description, multiple = false, accept = [] } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <div
			{..._.omit(this.props, [ 'onChange' ])}
			className={classNames(styles[ 'file-input' ], className, { [ styles[ 'required' ] ]: required })}
		>
			<label className={styles[ 'label' ]}>{floatingLabelText}</label>
			<div className="group-vertical">
				{
					!_.isEmpty(value) && _.isArray(value) &&
					_.map(value, (file, index) => {
						const fileType = _.get(file, 'type');
						const fileName = _.get(file, 'filename');
						const src = _.get(file, [ 'url' ]) || _.get(file, [ 'b64Data' ]);

						let node;
						switch ( true ) {
							case _.startsWith(fileType, 'image'):
								node = <img key={index} src={src} alt=""/>;
								break;
							case _.endsWith(fileType, 'pdf'):
								node = <FontAwesomeIcon icon={faFilePdf}/>;
								break;
							case _.endsWith(fileType, 'excel'):
							case _.endsWith(fileType, 'sheet'):
								node = <FontAwesomeIcon icon={faFileExcel}/>;
								break;
							case _.endsWith(fileType, 'document'):
							case _.endsWith(fileType, 'msword'):
								node = <FontAwesomeIcon icon={faFileWord}/>;
								break;
							default:
								node = <FontAwesomeIcon icon={faFile}/>;
						}

						return <div
							key={src}
							className={styles[ 'thumbnail' ]}
						>
							{node}
							<div className={styles[ 'desc' ]}>
								Filename: {fileName}
								<IconButton
									className={styles[ 'icon' ]}
									onTouchTap={ev => this.onReset(ev, file)}
								>
									<ClearIcon/>
								</IconButton>
							</div>
						</div>;
					})
				}
			</div>
			<div className="group-horizontal">
				<RaisedButton
					label="Choose a file"
					onTouchTap={() => this.refs.upload.click()}
					className={styles[ 'button' ]}
				>
					<input
						type="file"
						ref="upload"
						className={styles[ 'hidden-input' ]}
						multiple={multiple}
						accept={_.join(accept, ',')}
						onChange={(ev) => _.isFunction(this.onChange) && this.onChange(ev, ev.target.files)}
					/>
				</RaisedButton>
			</div>
		</div>;
	},
});

export default FileInput;