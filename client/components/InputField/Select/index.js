import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";

import { MenuItem } from 'material-ui';
import { FormsySelect } from 'formsy-material-ui/lib';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Select extends PureComponent {

	static propTypes = _.assign({}, FormsySelect.propTypes, {
		enums: PropTypes.array.isRequired,
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		multiple: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string
	});

	render() {
		const { required, multiple, description, className, title } = this.props;
		let { enums = [] } = this.props;

		if ( !required && !multiple ) {
			enums = [ { value: '', title: '' }, ...enums ];
		}

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <FormsySelect
			floatingLabelFixed={true}
			hintText={'Enter value'}
			fullWidth={true}
			floatingLabelText={floatingLabelText}
			autoWidth={true}
			{...this.props}
			className={classNames(styles[ 'select' ], className, {
				[ styles[ 'required' ] ]: required,
				[ styles[ 'description' ] ]: _.isString(description)
			})}
		>
			{
				_.map(enums, (item, index) => {
					return <MenuItem
						key={index}
						value={item.value}
						primaryText={item.title}
						disabled={item.disabled}
					/>;
				})
			}
		</FormsySelect>;
	}
}