import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import _ from 'lodash';
import Date from './Date/index';
import Text from './Text/index';
import Select from "./Select/index";
import Checkbox from "./Checkbox/index";
import DateTime from "./DateTime/index";
import Time from "./Time/index";
import Toggle from "./Toggle/index";
import FileInput from "./FileInput/index";
import { shiftTzDateToPickerDate, shiftPickerDateToTZDate } from "../../utils/time";

class InputField extends PureComponent {

	static propTypes = {
		field: PropTypes.objectOf({
			key: PropTypes.oneOfType([ PropTypes.string, PropTypes.func ]),
			title: PropTypes.oneOfType([ PropTypes.string, PropTypes.func ]),
			render: PropTypes.func,
			renderView: PropTypes.func,
			type: PropTypes.oneOfType([ PropTypes.string, PropTypes.func ]),
			isOptional: PropTypes.oneOfType([ PropTypes.bool, PropTypes.func ]),
			description: PropTypes.oneOfType([ PropTypes.string, PropTypes.func ]),
			multiple: PropTypes.oneOfType([ PropTypes.bool, PropTypes.func ]),
			enum: PropTypes.oneOfType([ PropTypes.array, PropTypes.func ]),
			maxLength: PropTypes.oneOfType([ PropTypes.number, PropTypes.func ]),
			validations: PropTypes.oneOfType([ PropTypes.object, PropTypes.func ]),
			validationErrors: PropTypes.oneOfType([ PropTypes.object, PropTypes.func ]),
			defaultValue: PropTypes.any,
			readOnly: PropTypes.oneOfType([ PropTypes.bool, PropTypes.func ]),
			needRender: PropTypes.oneOfType([ PropTypes.bool, PropTypes.func ]),
			accept: PropTypes.oneOfType([ PropTypes.func, PropTypes.array ]),
		}).isRequired,
		item: PropTypes.object,
		onChangeValue: PropTypes.func,
	};

	static contextTypes = {
		muiTheme: PropTypes.object.isRequired,
	};

	shouldComponentUpdate(nextProps) {
		return !_.isEqual(this.props.field, nextProps.field)
			|| !_.isEqual(this.props.item, nextProps.item);
	}

	componentDidMount() {
		const { field = {}, item = {} } = this.props;

		const {
			key, type, 'enum': enumerable = [], defaultValue = _.includes([ 'toggle', 'checkbox' ], type) ? false : undefined,
		} = _.mapValues(_.cloneDeep(field), (v, k) => k !== 'onChange' && _.isFunction(v) ? v(item, field) : v);

		let value = _.get(item, key, defaultValue);

		if ( !_.isFunction(this.onChangeValue) ) {
			return;
		}

		if ( !_.isUndefined(defaultValue) && _.isUndefined(item[ key ]) ) {
			switch ( type ) {
				case 'select':
					this.onChangeValue(null, value);
					break;
				case 'checkbox':
					this.onChangeValue(null, _.get(_.find(enumerable, (v) => v.key === value), 'value', value));
					break;
				case 'toggle':
					this.onChangeValue(null, JSON.parse(value));
					break;
				case 'date':
					value = moment(value).startOf('day').toDate();
				case 'datetime':
				case 'time':
					this.onChangeValue(null, shiftPickerDateToTZDate(value));
					break;
				default:
					this.onChangeValue(null, value);
			}
		}
	}

	onReset(ev) {
		const { onChangeValue } = this.props;
		_.isFunction(onChangeValue) && onChangeValue(ev, null);
	}

	onChangeValue(ev, value) {
		const { onChangeValue } = this.props;
		_.isFunction(onChangeValue) && onChangeValue(...arguments);
	}

	render() {
		const { field = {}, item = {} } = this.props;

		const mappedField = _.mapValues(_.cloneDeep(field), (v, k) => k !== 'onChange' && _.isFunction(v) ? v(item, field) : v);

		const {
			key, title, render, type, isOptional, description, multiple, 'enum': enumerable = [],
			maxLength, defaultValue, readOnly, needRender = true, accept = []
		} = mappedField;

		if ( !needRender ) {
			return null;
		}

		if ( !_.isUndefined(render) ) {
			return render;
		}

		let { validations = {}, validationErrors = {} } = mappedField;

		let value = _.get(item, key, defaultValue);
		const isRequired = !isOptional;

		let labelTitle = `${title}(${key})`;

		const validationNotEmpty = _.includes([ 'date', 'datetime', 'time', 'files' ], type)
			&& _.assign({}, validations, isRequired && { isExisty: true });

		const validationErrorsNotEmpty = _.includes([ 'date', 'datetime', 'time' ], type)
			&& _.assign({}, validations, isRequired && { isExisty: 'This field is required!' });

		switch ( type ) {
			case 'select':
				return <Select
					name={key}
					validations={validations}
					validationsErrors={validationErrors}
					required={isRequired}
					disabled={readOnly}
					value={value}
					enums={enumerable}
					multiple={!!multiple}
					description={description}
					title={labelTitle}
					onChange={this.onChangeValue.bind(this)}
				/>;

			case 'checkbox': {

				const isChecked = _.isBoolean(value) ? value : _.get(_.find(enumerable, (v) => v.value === value), 'key', false);

				return <Checkbox
					name={key}
					validations={validations}
					required={isRequired}
					validationsErrors={validationErrors}
					disabled={readOnly}
					value={isChecked}
					description={description}
					title={labelTitle}
					onChange={(ev, value) => this.onChangeValue(ev,
						_.get(_.find(enumerable, v => v.key === value), 'value', value))}
				/>;
			}

			case 'date': {
				let fieldValue = !!value && !_.isDate(value) ? shiftPickerDateToTZDate(moment(value).startOf('day').toDate()) : value;

				return <Date
					name={key}
					validations={validationNotEmpty}
					validationErrors={validationErrorsNotEmpty}
					required={isRequired}
					disabled={readOnly}
					title={labelTitle}
					description={description}
					value={shiftTzDateToPickerDate(fieldValue)}
					onChange={(ev, date) => _.isFunction(this.onChangeValue)
						&& this.onChangeValue(ev, shiftPickerDateToTZDate(moment(date).startOf('day').toDate()))}
					onReset={(ev) => this.onReset(ev)}
				/>;
			}

			case 'datetime': {
				let fieldValue = !!value && !_.isDate(value) ? shiftPickerDateToTZDate(moment(value).toDate()) : value;

				return <DateTime
					name={key}
					validations={validationNotEmpty}
					validationErrors={validationErrorsNotEmpty}
					required={isRequired}
					disabled={readOnly}
					floatingLabelFixed={true}
					title={labelTitle}
					description={description}
					value={shiftTzDateToPickerDate(fieldValue)}
					onChange={(ev, date) => _.isFunction(this.onChangeValue) && this.onChangeValue(ev, shiftPickerDateToTZDate(date))}
					onReset={(ev) => this.onReset(ev)}
				/>;
			}

			case 'time': {
				let fieldValue = !!value && !_.isDate(value) ? shiftPickerDateToTZDate(moment(value).toDate()) : value;

				return <Time
					name={key}
					validations={validationNotEmpty}
					validationErrors={validationErrorsNotEmpty}
					required={isRequired}
					disabled={readOnly}
					floatingLabelFixed={true}
					title={labelTitle}
					description={description}
					value={shiftTzDateToPickerDate(fieldValue)}
					onChange={(ev, date) => _.isFunction(this.onChangeValue) && this.onChangeValue(ev, shiftPickerDateToTZDate(date))}
					onReset={(ev) => this.onReset(ev)}
				/>;
			}

			case 'toggle': {
				return <Toggle
					name={key}
					disabled={readOnly}
					required={isRequired}
					value={value}
					title={labelTitle}
					description={description}
					onChange={this.onChangeValue.bind(this)}
				/>;
			}

			case 'files': {
				return <FileInput
					name={key}
					value={value}
					fullWidth={true}
					required={isRequired}
					description={description}
					validations={validationNotEmpty}
					validationErrors={validationErrors}
					title={labelTitle}
					multiple={!!multiple}
					onChange={(ev, images) => this.onChangeValue(ev, images)}
					onReset={(ev) => this.onReset(ev)}
					accept={accept}
				/>;
			}

			default: {

				validations = _.assign({}, validations,
					isRequired && { isNotEmpty: true },
					type === 'number' && { isNumeric: true },
					type === 'email' && { isEmail: true },
					_.isNumber(maxLength) && { maxLength: maxLength },
				);

				validationErrors = _.assign({}, validationErrors,
					type === 'number' && { isNumeric: 'Only digits are valid.' },
					type === 'email' && { isEmail: 'Not valid email.' },
					_.isNumber(maxLength) && { maxLength: `Field's length should be less than ${maxLength} symbols.` },
				);

				return <Text
					validations={validations}
					validationErrors={validationErrors}
					type={_.isUndefined(type) || type === 'number' ? 'search' : type}
					required={isRequired}
					name={key}
					title={labelTitle}
					description={description}
					autoComplete="off"
					disabled={readOnly}
					multiLine={type === 'textarea'}
					rows={type === 'textarea' ? 5 : 1}
					value={value}
					onChange={this.onChangeValue.bind(this)}
				/>;
			}
		}
	}

}

export default InputField;
