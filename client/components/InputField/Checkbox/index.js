import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames';

import FormsyCheckbox from 'formsy-material-ui/lib/FormsyCheckbox';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Checkbox extends PureComponent {
	static contextTypes = {
		muiTheme: PropTypes.object.isRequired,
	};

	static propTypes = _.assign({}, FormsyCheckbox.propTypes, {
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string
	});

	render() {
		const { description, value, className, required, title } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		let isChecked;
		try {
			isChecked = value && JSON.parse(value);
		} catch ( ex ) {
			isChecked = false;
		}

		const iconStyle = {
			fill: isChecked ? this.context.muiTheme.checkbox.checkedColor : 'rgba(0, 0, 0, 0.3)'
		};

		return <FormsyCheckbox
			labelPosition="left"
			iconStyle={iconStyle}
			fullWidth={true}
			label={floatingLabelText}
			{...this.props}
			className={classNames(styles[ 'checkbox' ], className, { [ styles[ 'required' ] ]: required })}
		/>;
	}
}