import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";

import FormsyText from 'formsy-material-ui/lib/FormsyText';

import styles from './styles.scss';
import InfoIcon from "material-ui/svg-icons/action/info-outline";

export default class Text extends PureComponent {

	static propTypes = _.assign({}, FormsyText.propTypes, {
		required: PropTypes.bool,
		disabled: PropTypes.bool,
		description: PropTypes.string,
		title: PropTypes.string
	});

	render() {

		const { required, description, className, title, value } = this.props;

		const floatingLabelText = (
			<span>
			{title}
				{
					_.isString(description) &&
					<span title={description} style={{ cursor: 'help' }}>
						<InfoIcon style={{ color: 'inherit' }}/>
					</span>
				}
			</span>
		);

		return <FormsyText
			floatingLabelFixed={true}
			hintText={'Enter value'}
			fullWidth={true}
			floatingLabelText={floatingLabelText}
			{...this.props}
			value={_.toString(value)}
			className={classNames(styles[ 'text' ], className, {
				[ styles[ 'required' ] ]: required,
				[ styles[ 'description' ] ]: _.isString(description)
			})}
		/>;
	}
}
