import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state, props) => {
  return {
    theme: state.common.theme,
    amountOfActiveProcesses: state.common.amountOfActiveProcesses,
  };
};

export default function (descriptor) {
  if (descriptor.kind === 'class') {
    descriptor.finisher = connect(mapStateToProps, mapDispatchToProps);
  } else {
    throw new Error('Unsupported decorated element');
  }
}