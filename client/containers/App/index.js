import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import connect from './connect';
import { localization } from '../../localization';
import { Route, Switch } from "react-router-dom";
import classNames from 'classnames';
import './styles.scss';
import _ from 'lodash';

@connect
@localization
export default class App extends PureComponent {
  static propTypes = {
    children: PropTypes.any,
    response: PropTypes.objectOf({
      message: PropTypes.string,
      type: PropTypes.string,
    }),
    amountOfActiveProcesses: PropTypes.number,
  };

  state = {};

  render() {
    let {
      children,
      amountOfActiveProcesses,
      theme,
      t,
    } = this.props;

    const refreshIndicatorStatus = amountOfActiveProcesses ? 'loading' : 'hide';

    return (
      <div className={classNames('application', theme)}>
        {t('hello')}
        {children}
        <Switch>
          {/*<Route exact path="/" component={Home} />*/}
          {/*<Route path="/about" component={About} />*/}
          {/*<Route path="/contact" component={Contact} />*/}
        </Switch>
      </div>
    );
  }

}