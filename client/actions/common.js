import { createAction } from 'redux-act';

export default {
  clearResponse: createAction('CLEAR_RESPONSE'),

  catchResponse: createAction('CATCH_RESPONSE'),

  addProcess: createAction('ADD_PROCESS'),

  removeProcess: createAction('REMOVE_PROCESS'),
};
