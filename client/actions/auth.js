import { createActionAsync } from 'redux-act-async';
import axios from 'axios';

export default {
  login: createActionAsync('AUTH_LOGIN', ({ email, password }) => {
    return axios({
      method: 'post',
      url: '/api/auth/login',
      data: { email, password },
    });
  }),

  logout: createActionAsync('AUTH_LOGOUT', () => {
    return axios({
      method: 'get',
      url: '/api/auth/logout',
    });
  }),
};
