import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from "react-router-dom";
import configureStore from './store/configureStore';
import './localization';
import App from './containers/App';

// Grab the state from a global injected into
const initialState = window.__INITIAL_STATE__;

// Allow the passed state to be garbage-collected
delete window.__INITIAL_STATE__;

const store = configureStore(initialState);
// redirect to the /login page if user is not authenticated
// const requireAuth = (nextState, replace, callback) => {
//   const authUser = store.getState().getIn([ 'auth', 'user' ]);
//   if ( authUser.isEmpty() ) {
//     replace({
//       pathname: '/login',
//       state: { nextPathname: nextState.location.pathname },
//     });
//   }
//   callback();
// };

// redirect to the /admin page if user is authenticated
// const redirectAuth = (nextState, replace, callback) => {
//   const authUser = store.getState().getIn([ 'auth', 'user' ]);
//   if ( !authUser.isEmpty() ) {
//     replace({
//       pathname: '/',
//     });
//   }
//   callback();
// };

function renderAdvanced(Component) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Component/>
      </BrowserRouter>
    </Provider>, document.getElementById('app'),
  );
}

renderAdvanced(App);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const appComponent = require('./containers/App').default;
    renderAdvanced(appComponent);
  });
}