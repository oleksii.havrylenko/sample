import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import Auth from '../../shared/auth/index';

export default (WrappedComponent, componentName) => {
  class CheckPermissions extends Component {

    componentWillMount() {
      this.AuthCtrl = Auth(_.isFunction(componentName) ? componentName(this.props) : componentName);
    }

    componentWillReceiveProps(nextProps) {
      if (!_.isEqual(this.props.location.pathname, nextProps.location.pathname)) {
        this.AuthCtrl = Auth(_.isFunction(componentName) ? componentName(nextProps) : componentName);
      }
    }

    render() {
      const { user = {} } = this.props;

      return <WrappedComponent
        canCreate={this.AuthCtrl.webCheckPermissionOn('create', user)}
        canEdit={this.AuthCtrl.webCheckPermissionOn('update', user)}
        canDelete={this.AuthCtrl.webCheckPermissionOn('delete', user)}
        canImport={this.AuthCtrl.webCheckPermissionOn('import', user)}
        canRestart={this.AuthCtrl.webCheckPermissionOn('restart', user)}
        canStop={this.AuthCtrl.webCheckPermissionOn('stop', user)}
        {...this.props}
      />;
    }
  }

  const mapStateToProps = (state) => {
    return {
      user: _.get(state, ['auth', 'user']),
    };
  };

  return connect(mapStateToProps)(CheckPermissions);
}