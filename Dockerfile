FROM node:10.15.3-alpine

ARG BUILD_MODE=production

ENV HOME_DIR /home/node
ENV DIR $HOME_DIR/sample

WORKDIR $DIR
ENTRYPOINT ["/bin/sh"]
EXPOSE 3000
CMD [ "./wait-for-postgres.sh", "npm run start" ]

COPY . $DIR

RUN apk --update --no-cache add alpine-sdk postgresql python py-pip \
    && pip install awscli --upgrade \
    && npm -s i \
    && npm rebuild bcrypt --build-from-source \
    && npm run build \
    && npm -s cache clean --force \
    && [[ $BUILD_MODE = "production" ]] \
        && npm -s prune --production \
        && apk del alpine-sdk py-pip \
    || echo "$BUILD_MODE mode" \
    && chown -R node:node $HOME_DIR

USER node